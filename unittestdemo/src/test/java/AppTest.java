//package dawson;
// I commented out the "package dawson;" because with it I was receiving an error

import static org.junit.Assert.*;

import org.junit.Test;

public class AppTest {
    
    @Test
    public void shouldAnswerWithTrue(){
        assertTrue( true );
    }

    @Test
    public void testingEcho(){
        assertEquals( "Testing the echo() method from the App class", 5, dawson.App.echo(5) );
    }

    @Test
    public void testingOneMore(){
        assertEquals( "Testing the oneMore() method from the App class", 6, dawson.App.oneMore(5)); // This test is now correct since i corrected the bug in the App class
    }

}