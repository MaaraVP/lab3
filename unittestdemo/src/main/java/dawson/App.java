package dawson;

public class App {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

    public static int echo(int x){
        return x;
    }

    public static int oneMore(int x){
        int y = x + 1;
        return y; // This used to be "return x;" but I corrected the purposely made bug
    }

}